# MOVED TO [GITHUB](https://github.com/nik9play/flexpip)

# flexPiP
Tool for watching videos in PiP mode.

![Screenshot](https://i.imgur.com/LtSVGzD.png)

# Build

```console
git clone https://gitlab.com/nik9play/flexpip.git
yarn
yarn dist
```
# Download

## Windows
[Portable](https://megaworldnetwork.ru/flexpip/flexpip-latest.exe)
## Linux
[AppImage](https://megaworldnetwork.ru/flexpip/flexpip-latest.AppImage)
[snap](https://megaworldnetwork.ru/flexpip/flexpip-latest.snap)
[Debian-based dists](https://megaworldnetwork.ru/flexpip/flexpip-latest.deb)
## macOS
[DMG](https://megaworldnetwork.ru/flexpip/flexpip-latest.dmg)